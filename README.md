ascii protocol

https://mmrc.caltech.edu/Kratos%20XPS/MKS%20RGA/ASCII%20Protocol%20User%20Manual%20SP1040016.100.pdf

https://mmrc.caltech.edu/Kratos%20XPS/MKS%20RGA/ASCII%20Protocol%20User%20Manual%20SP1040016.100.pdf

https://mmrc.caltech.edu/Kratos%20XPS/MKS%20RGA/RGA%20SDK%20User%20Guide%20SP104015.100.pdf

https://mmrc.caltech.edu/Kratos%20XPS/MKS%20RGA/MKS.html

https://mmrc.caltech.edu/Kratos%20XPS/MKS%20RGA/ASCII%20Protocol%20User%20Manual%20SP1040016.100.pdf

https://epics.anl.gov/tech-talk/2016/msg00104.php

https://adms.fnal.gov/vacuum/manuals/mksrga/Manuals/Software/LP102027.100%20Using%20Virtual%20RGA%20Systems.pdf

htpps://<hostname>/pages/scripts/mv2isapi.dll/index.htm


example::

import tango
dp = tango.DeviceProxy('bl16/vc/rga-01')

dp.FIL_ON()
dp.PointsPerMass = 8
dp.MassUpTo = 20
dp.StartBarScan()
dp.PointsPerMass = 8
dp.MassUpTo = 20
dp.MassStartFrom = 2
dp.FIL_ON()
dp.FilamentState()
dp.FilamentState
dp.StartBarScan()
len(dp.AnalogMasses)
len(dp.AnalogPressures)
dp.StopScan()
dp.StartAnalogScan()
dp.FIL_ON()
dp.MassUpTo = 20
dp.MassStartFrom = 2
dp.PointsPerMass = 8
dp.StartBarScan()
dp.Accuracy = 8
dp.StopScan()
dp.StartBarScan()
dp.StopScan()
dp.Accuracy = 8
dp.Accuracy = 8
dp.PointsPerMass = 8
dp.MassUpTo = 20
dp.MassStartFrom = 2
dp.Accuracy
dp.PointsPerMass = 8
dp.FIL_ON()
dp.StartBarScan()
dp.FIL_ON()
dp.PointsPerMass = 8
dp.MassStartFrom = 2
dp.MassUpTo = 20
dp.StartBarScan()
dp.StopScan()
dp.StartAnalogScan()
dp.StopScan()
dp.PeakScanAddMass([1,1,0,1])
dp.PeakScanAddMass([8,1,0,1])
dp.PeakScanAddMass([18,1,0,1])
dp.StartPeakScan()
dp.PeakScanAddMass([1,1,0,1])
dp.PeakScanAddMass([18,1,0,1])
dp.StartPeakScan()
dp.FIL_ON()
dp.MassStartFrom = 2
dp.MassUpTo = 20
dp.PointsPerMass = 8
dp.StartAnalogScan()
dp.StopScan()
dp.StartBarScan()
dp.StopScan()
dp.PeakScanAddMass([2,1,0,1])
dp.PeakScanAddMass([24,1,0,1])
dp.PeakScanAddMass([2,1,0,1])
dp.PeakScanAddMass([24,1,0,1])
dp.StartPeakScan()
dp.StopScan()



