#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# ##### BEGIN GPL LICENSE BLOCK #####
#
# MyDevice
# Copyright (C) 2022  Emilio José Morales Alejandre
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


from setuptools import setup, find_packages

# The version is updated automatically with bumpversion
# Do not update manually!!
__version__ = '0.1.0'

def main():

    setup(
    	name='mksRGA',
    	version=__version__,
        package_dir={'mksRGA': 'rga'},
        packages=find_packages(),
        author="Lluis Ginés",
        author_email="lgines@cells.es",
        description="Tango Device Server for MKS-RGA",
        license='GPLv3+',
        url='https://gitlab.com/LluisG/rga-integrator',
        requires=['tango (>=8.1.0)'],
        entry_points={
            "console_scripts": ['MKSRGA = rga.mksRGA:main'],
        },
    )

if __name__ == "__main__":
    main()

