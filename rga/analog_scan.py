
"""could be deleted"""

import logging
import threading
import numpy as np

try:
    """for console"""
    from .attributes import Attributes
    from .record_scan import AnalogRecord
except:
    """for DS"""
    from attributes import Attributes
    from record_scan import AnalogRecord


class AnalogScan:
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(threadName)s - '
                               ' %(levelname)s - %(message)s')

    def __init__(self, send):

        self.analog_scan_stop = threading.Event()
        self.send = send
        log_name = '{}Record'.format(__name__)

        self.log = logging.getLogger(log_name)

        self.start_mass = 2
        self.end_mass = 5
        self.points = 4
        self.accuracy = 2
        self.gain = 0
        self.source = 0
        self.detector = 0

        self.analog_settings = [
            self.start_mass,
            self.end_mass,
            self.points,
            self.accuracy,
            self.gain,
            self.source,
            self.detector
        ]




        self.record = AnalogRecord()
        self.bunch_scan = np.array([])

        self.recording = False

        self.masses_list = np.array([])
        self.pressure_list = np.array([])

        self.do_lists()

        self.lock = threading.Lock()



    def send_cmd(self, cmd):
        self.send.send(cmd)

    def do_lists(self):
        self.masses_list = np.arange(self.analog_settings[0],
                                     self.analog_settings[1] + 1,
                                     1 / self.analog_settings[2])

        self.pressure_list = np.zeros(len(self.masses_list))

        #
        # for i in range(len(self.masses_list)):
        #     self.pressure_list[i]=0

        return self.masses_list, self.pressure_list

    def start_analog(self):

        analog_tr = threading.Thread(target=self.analog_loop)
        analog_tr.start()

    def analog_loop(self):
        self.analog_scan_stop.clear()
        n_scan = 0
        n_mass = 0
        while not self.analog_scan_stop.isSet():
            #
            # if self.analog_settings[0] != self.old_start_mass or \
            #         self.analog_settings[1] != self.old_end_mass:
            #     self.old_start_mass = self.analog_settings[0]
            #     self.old_end_mass = self.analog_settings[1]
            #     self.do_lists()

            # r = self.send.read()
            str_to_send = 'AddAnalog analog1 '
            for i in self.analog_settings:
                i = str(i)
                str_to_send += i + ' '

            print(str_to_send)
            self.send_cmd(str_to_send + '')

            self.send_cmd('ScanAdd analog1')
            self.send_cmd('ScanStart 1')
            r = self.send.read()

            while not r.find("MassReading") > -1:
                r = self.send.read()
                n_mass = 0
            while r.find("Mass") > -1:
                r = r.split(" ")
                pressure = float("{:.2e}".format(float(r[2])))
                print("PPP",pressure)
                print("nnn", n_mass)
                with self.lock:
                    self.pressure_list[n_mass] = pressure

                n_mass += 1
                # self.masses_list = np.arange(mass, pressure)

                # if mass % 1 == 0:

                # self.bunch_scan.append(self.line + "\n")
                #
                #     self.masses_list = np.arange(mass, pressure)

                # with self.lock:
                #     self.bunch_scan.append(self.line + "\n")

                r = self.send.read()

            # if not r:
            self.log.info('Scan Done')
            # print("BUNCH:", self.bunch_scan)
            # print('n_scan= ', n_scan)
            # print(self.attributes.mass_attr)
            if self.recording:
                self.save_analog_line(self.bunch_scan)

            # self.bunch_scan = []
            self.send.send('ScanStop')
            self.send_cmd('MeasurementRemoveAll')
            n_scan += 1

        print("ANALOG STOPPED!!")
        self.send.send('ScanStop')
        self.send_cmd('MeasurementRemoveAll')
        self.analog_scan_stop.clear()

    # def get_scan(self):
    #     # scan = np.array([self.masses_list, self.pressure_list])
    #
    #     return self.masses_list ,self.pressure_list

    def get_masses(self):
        return self.masses_list

    def get_pressures(self):
        return self.pressure_list

    """analog record loop"""

    def start_analog_record(self, file_name):

        self.record.settings = self.analog_settings
        self.record.create_file(file_name)
        self.recording = True

    def save_analog_line(self, line):
        self.record.save_analog_line(line)
