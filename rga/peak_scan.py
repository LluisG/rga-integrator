import logging
import threading
from array import array
import time
import numpy as np

"""for console"""
try:
    from .peak_record import PeakRecord
    # from .controller import RGAController

except:
    """for DS"""
    from peak_record import PeakRecord
    # from controller import RGAController


class PeakScan:
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(threadName)s - '
                               ' %(levelname)s - %(message)s')

    def __init__(self, send):

        self.peak_scan_stop = threading.Event()
        self.send = send
        log_name = '{}Record'.format(__name__)
        self.log = logging.getLogger(log_name)

        # self.send.send('MeasurementRemoveAll')
        # self.mass_settings = np.array([])

        # self.scan_settings = np.array([])
        self.scan_settings = []
        self.n_mass = 1

        self.record = PeakRecord()
        self.recording = False

        self.lock = threading.Lock()
        self.peak_masses = []
        self.peak_detectors = []
        self.peak_accuracies = []
        self.peak_gains = []
        # self.peak_pressures = []
        self.scan = [], []

        # self.availableScans = 10

        self.peak_parametersnp = np.zeros(([1, 5]), dtype=int)
        # self.peak_pressuresnp = np.zeros((10), dtype=float)

        # self.peak_parametersnp=np.array([])

        print("PEAKPARAMETERS_1", self.peak_parametersnp)

        """to test empy array"""
        # self.pressures_list = np.array([np.nan])
        self.pressures_list = np.array([])

    def send_cmd(self, cmd):
        self.send.send(cmd)

    def start_peak_scan(self):
        peak_tr = threading.Thread(target=self.peak_loop)
        peak_tr.start()

    def get_full_scan(self):
        scan = self.peak_masses, self.pressures_list
        print("SCAN = ", scan)
        return scan

    def get_pressures(self):
        # print(self.pressure)
        # return self.pressure

        # return self.peak_pressuresnp

        """to test  empy array"""
        with self.lock:
            return self.pressures_list

    # def get_masses(self):
    #     self.peak_masses = []
    #     for i in range(len(self.scan_settings)):
    #         self.peak_masses.append(self.scan_settings[i][0])
    #
    #     return self.peak_masses

    def get_masses_np(self):
        return self.peak_masses

    def get_detectors(self):
        for i in range(len(self.scan_settings)):
            self.peak_detectors.append(self.scan_settings[i][1])
        return self.peak_detectors

    def get_accuracies(self):
        for i in range(len(self.scan_settings)):
            self.peak_accuracies.append(self.scan_settings[i][2])
        return self.peak_accuracies

    def get_gains(self):
        for i in range(len(self.scan_settings)):
            self.peak_gains.append(self.scan_settings[i][3])
        return self.peak_gains


    def add_mass(self, settings):
        settings = np.insert(settings, 3, 0)
        newMass=np.array([settings])
        with self.lock:
            self.peak_parametersnp = np.append(self.peak_parametersnp, newMass,
                                         axis=0)
        print(self.peak_parametersnp)

    def remove_mass(self, mass):
        print("before",self.peak_parametersnp )
        with self.lock:
            self.peak_parametersnp=np.delete(self.peak_parametersnp,np.where
            (self.peak_parametersnp[:,0]==mass)[0],axis=0)

        print("after",self.peak_parametersnp )


    def remove_all(self):
        with self.lock:
            self.peak_parametersnp = np.zeros(([1, 5]), dtype=int)
            self.send.send('MeasurementRemoveAll')
            return 'All masses removed'

    def start_peak_record(self, file_name):

        self.record.settings = self.scan_settings
        self.record.create_file(file_name)
        self.recording = True

    def save_peak_line(self, line):
        self.record.save_line(line)



    def peak_loop(self):
        # self.remove_all()
        # self.pressures_list=np.zeros(np.count_nonzero(self.peak_masses))
        print("PRESSURESLIST", self.pressures_list)
        print(self.peak_parametersnp)
        self.peak_scan_stop.clear()
        if not np.any(self.peak_parametersnp):
            self.log.info('No masses added to the scan')
        else:
            print("PEAKPARAMETERS_2", self.peak_parametersnp)
            """this removes the ZERO rows"""
            print(self.peak_parametersnp)
            self.peak_parametersnp = self.peak_parametersnp[~np.all
            (self.peak_parametersnp == 0, axis=1)]
            print(self.peak_parametersnp)
            self.pressures_list = np.zeros(len(self.peak_parametersnp))
            self.log.info('Init Peak Scan Loop')
            while not self.peak_scan_stop.isSet():
                print("TIMEXX",int(time.time()))
                n = 0
                for i in self.peak_parametersnp:
                    print('SCAN ' +str(i))
                    massesList = ' '.join(str(s) for s in i)
                    strTosend = 'AddSinglePeak scanTodo ' + massesList
                    print("STR", strTosend)

                    self.send.send(strTosend)
                    self.send.send('ScanAdd scanTodo')
                    self.send.send("ScanStart 1 ",clean=False) #do not filter measurement values

                    r = "" #self.send.read()

                    # self.peak_masses[i] = self.scan_settings[i][0]

                    while not self.peak_scan_stop.isSet() and not r.find("MassReading") > -1:
                        r = self.send.read(clean=False)

                    try:
                        r = float(r.split(" ")[-1])
                    except:
                        print('Invalid reading: %s' % str(r))
                        r = 0.
                    #r = float("{:.2e}".format(float(r[2])))
                    # r=float("%.2e"%r)
                    print("R= ", r)

                    # with self.lock:
                    # self.scan=self.peak_masses[i],[r]
                    # TODO find index of the corresponding mass row
                    # n = np.where(
                    #     np.all(self.peak_parametersnp == i, axis=1))
                    #

                    # print("N ", n)
                    print("N=", n)
                    # self.peak_pressuresnp[n] = r

                    # self.peak_pressuresnp[n] = r
                    with self.lock:
                        self.pressures_list[n] = r

                    # print("Pressures", self.peak_pressuresnp)
                    print("PRESSURES", self.pressures_list)

                    n += 1
                    self.send.send('ScanStop')
                    self.send.send('MeasurementRemoveAll')

                    # print("SCAN=", self.peak_masses, self.pressure)

                # n = 0
            self.peak_scan_stop.clear()
            # self.send.send('MeasurementRemoveAll')
            print("PEAK STOPPED!!")

    def stopScan(self):
        self.peak_scan_stop.set()

