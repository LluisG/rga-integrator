import time
import traceback

import numpy as np
import tango

"""for console"""
# try:
#     from .tango import AttrQuality, AttrWriteType, DispLevel, DevState, DebugIt
#     from .tango.server import Device, attribute, command, pipe, device_property
#     from .controller import RGAController
#     from .threading import Thread
#     """for DS"""
# except:
from tango import AttrQuality, AttrWriteType, DispLevel, DevState, DebugIt, \
    SPECTRUM, IMAGE
from tango.server import Device, attribute, command, pipe, device_property
from .controller import RGAController
from threading import Thread


class MKSRGA(Device):
    host = device_property(dtype=str)
    filament_check_period = device_property(dtype=int, default_value=30)
    port = device_property(dtype=int, default_value=10014)

    def init_device(self):
        Device.init_device(self)
        self.set_state(DevState.STANDBY)
        try:
            self.rga = RGAController(self.host, self.port)
        except:
            traceback.print_exc()
            self.set_state(DevState.UNKNOWN)



        # self._filament_number = self.rga.get_filament_number()
        # self._filament_info = self.rga.get_filament_state()

        """TODO check if csn be done by pooling"""
        # self._filament_state_thread = Thread(target=self._check_filament,
        #                                      daemon=True)
        #
        # self._filament_state_thread.start()

    # def _check_filament(self):
    #     time.sleep(0.5)
    #     while True:
    #          self.FilamentState()
    #
    #          time.sleep(self.filament_check_period)

    """ionizer / detector settings"""
    @attribute(label="Async", dtype=str)
    def Async(self):
        return self.rga.read()

    @command(dtype_in = str, dtype_out = str)
    def SendCommand(self, cmd):
        #print('Sending: %s' % cmd)
        cmd = self.rga.send(cmd)
        #print('Received:\n%s' % cmd)
        return cmd

    @command()
    def GainControl(self):
        self.rga.gain_control()

    @attribute(label="MaxMass", dtype=int)
    def MaxMass(self):
        return self.rga.get_max_mass()

    @command()
    def ReleaseControl(self):
        self.rga.release_control()

    @attribute(label="Sensors", dtype=(str,), max_dim_x=6, max_dim_y=0)
    def Sensors(self):
        return self.rga.get_sensors()


    @attribute(label="Gains", dtype=(str,), max_dim_x=5, max_dim_y=0)
    def Gains(self):
        return self.rga.get_gains()

    @attribute(label="Gain Selected", dtype=int)
    def GainSelected(self):
        return self.rga.get_gain_in_use()

    @GainSelected.write
    def GainSelected(self, value):
        self.rga.set_gain_in_use(value)

    @attribute(label="Electron Energy", max_value=100, min_value=0, unit="eV",
               dtype=float)
    def ElectronEnergy(self):
        return self.rga.get_electron_energy()

    @ElectronEnergy.write
    def ElectronEnergy(self, value):
        self.rga.set_electron_energy(value)

    @attribute(label="Electron Emission", max_value=5, min_value=0, unit="mA",
               dtype=float)
    def ElectronEmission(self):
        return self.rga.get_electron_emission()

    @ElectronEmission.write
    def ElectronEmission(self, value):
        self.rga.set_electron_emission(value)

    @attribute(label="Ion Energy", max_value=10, min_value=0, unit="eV",
               dtype=float)
    def IonEnergy(self):
        return self.rga.get_ion_energy()

    @IonEnergy.write
    def IonEnergy(self, value):
        self.rga.set_ion_energy(value)

    @attribute(label="Extractor Voltage", max_value=0, min_value=-130,
               unit="V", dtype=float)
    def ExtractorVoltage(self):
        return self.rga.get_extractor_voltage()

    @ExtractorVoltage.write
    def ExtractorVoltage(self, value):
        self.rga.set_extraction_voltage(value)

    """Source """



    @attribute(label="Low mass alignment", dtype=int, min_value=0,
               max_value=65535)
    def LowMassAlignment(self):
        return self.rga.get_low_mass_alignment()

    @LowMassAlignment.write
    def LowMassAlignment(self, value):
        self.rga.set_low_mass_alignment(value)

    @attribute(label="High Mass Alignment", min_value=0, max_value=65535,
               dtype=int)
    def HighMassAlignment(self):
        return self.rga.get_high_mass_alignment()

    @HighMassAlignment.write
    def HighMassAlignment(self, value):
        self.rga.set_high_mass_alignment(value)

    @attribute(label="Low Mass Res", min_value=0, max_value=65535, dtype=int)
    def LowMassResolution(self):
        return self.rga.get_low_mass_resolution()

    @LowMassResolution.write
    def LowMassResolution(self, value):
        self.rga.set_low_mass_resolution(value)

    @attribute(label="High Mass Res", min_value=0, max_value=65535, dtype=int)
    def HighMassResolution(self):
        return self.rga.get_high_mass_resolution()

    @HighMassResolution.write
    def HighMassResolution(self, value):
        self.rga.set_high_mass_resolution(value)

    @attribute(label="max pressure recommended", unit="mbar", dtype=float)
    def maxPressureRecommended(self):
        return self.rga.get_max_pressure_recommended()

    @attribute(dtype=str)
    def Diagnostic(self):
        return self.rga.get_diagnostic()

    @attribute(dtype=str)
    def GeneralInfo(self):
        return self.rga.get_general_info()

    """filament management"""

    @attribute(label="n filament", dtype=int)
    def FilamentSelected(self):
        return self.rga.get_filament_number()

    @command
    def SelectFil1(self):
        self.rga.set_filament1()
        self._filament_number = self.rga.get_filament_number()

    @command
    def SelectFil2(self):
        self.rga.set_filament2()
        self._filament_number = self.rga.get_filament_number()

    @attribute(label="Filament State", dtype=str)
    def FilamentState(self):
        return self.rga.get_filament_state()

    @command
    def Fil_ON(self):
        self.rga.set_filament_on()
        self._filament_info = self.rga.get_filament_state()


    @command
    def Fil_OFF(self):
        self.rga.set_filament_off()
        self._filament_info = self.rga.get_filament_state()


    """Degas"""

    @command
    def StopDegas(self):
        self.rga.set_degas_stop()

    @command(dtype_in=str)
    # def StartDegas(self, startPower, endPower, rampUP, rampDown, dwell):
    #     self.rga.set_degas_on(startPower, endPower, rampUP, rampDown, dwell)
    def StartDegas(self, params):
        self.rga.set_degas_on(params)

    @attribute(label="DegasInfo", dtype=str)
    def DegasInfo(self):
        return self.rga.degas_state()
    #
    """analog scan management"""

    @attribute(label="1st mass scan", unit="amu", dtype=int, min_value=1,
               max_value=100)
    def MassStartFrom(self):
        return self.rga.get_start_mass()

    @MassStartFrom.write
    def MassStartFrom(self, mass):
        self.rga.set_start_mass(mass)

    @attribute(label="last mass scan", unit="amu", dtype=int, min_value=1,
               max_value=100)
    def MassUpTo(self):
        return self.rga.get_end_mass()

    @MassUpTo.write
    def MassUpTo(self, mass):
        self.rga.set_end_mass(mass)

    @attribute(label="points per mass", dtype=int)
    def PointsPerMass(self):
        return self.rga.get_points_per_mass()

    @PointsPerMass.write
    def PointsPerMass(self, points):
        self.rga.set_points_per_mass(points)

    @attribute(label="Sensor Selected", dtype=int)
    def DetectorSelected(self):
        return self.rga.get_sensor_in_use()

    @DetectorSelected.write
    def DetectorSelected(self, value):
        self.rga.set_sensor_in_use(value)

    @attribute(label="Accuracy", dtype=int, min_value=-1,
               max_value=9)
    def Accuracy(self):
        return self.rga.get_accuracy()

    @Accuracy.write
    def SetAccuracy(self, value):
        self.rga.set_accuracy(value)

    @command
    def StartAnalogScan(self):
        self.rga.set_analog_scan_start()
        self.set_state(DevState.RUNNING)

    """peak scan /analog scan"""

    # @attribute(label="Peak masses", dtype=[int,], max_dim_x=(15))
    # def PeakMasses(self):
    #     self.rga.get_peak_scan_settings()

    @command
    def StartBarScan(self):
        self.rga.set_bar_scan_start()
        self.set_state(DevState.RUNNING)

    """Peak Scan Management"""

    @command
    def StartPeakScan(self):
        self.rga.set_peak_scan_start()
        self.set_state(DevState.RUNNING)

    @command(dtype_in=[int])
    def PeakScanAddMass(self, mass):
        self.rga.peak_scan_add_mass_np(mass)

    # @attribute(label="PeakScan", dtype=['float'],
    #            max_dim_x=15, max_dim_y=1)
    # def PeakScanPressures(self):
    #     return self.rga.get_peak_scan_measure()



    # @attribute()
    # def PeakPressures(self):
    #     return self.rga.get_peak_pressures()

    @attribute
    def PeakDetectors(self):
        return self.rga.get_peak_scan_detectors()

    @command
    def PeakAccuracies(self):
        return self.rga.get_peak_scan_accuracies()

    @attribute()
    def PeakGains(self):
        return self.rga.get_peak_scan_gains()

    # @command(dtype_out = ['float'])
    # def PeakMasses(self):
    #     return self.rga.get_peak_scan_masses()
    @attribute(dtype=['int'], max_dim_x=15, )
    def PeakMasses(self):
        return self.rga.get_peak_scan_massesnp()



    @command(dtype_in=int)
    def PeakScanRemoveMassIndex(self, index):
        return self.rga.peak_scan_remove_mass_by_index(index)

    @command(dtype_in=int)
    def peakScanRemoveMassWeight(self, amu):
        return self.rga.peak_scan_remove_mass_by_weight(amu)

    @command
    def PeakScanRemoveAll(self):
        return self.rga.peak_scan_remove_all()

    """scan results"""

    @attribute(label="PeakPartialPressure", unit="mbar", dtype=[float,], max_dim_x=3200,
               max_dim_y=2)
    def PeakPressures(self):
        return self.rga.get_peak_pressures()

    @attribute(label="AnalogPartialPressure",unit="mbar", dtype=[float],max_dim_x=3200,
               max_dim_y=1)
    def AnalogPressures(self):
        return self.rga.get_analog_pressures()

    @attribute(label="AnalogMasses", dtype=[float,],max_dim_x=3200,
               max_dim_y=5)
    def AnalogMasses(self):
        return self.rga.get_analog_masses()

    @attribute(label="PPressure", dtype=[[float]],max_dim_x=3200,
               max_dim_y=2)
    def AnalogFullScanPressures(self):
        return self.rga.get_analog_full_scan()
    @attribute(label="AnalogTimeScan")
    def AnalogTimeScan(self):
        return self.rga.get_time_scan()

    @attribute(label='WorkingMode', dtype=str)
    def WorkingMode(self):
        data = self.rga.working_mode
        return data        

    @command
    def StopScan(self):
        self.rga.stop_scan()
        self.set_state(DevState.STANDBY)

    """CIRRUS"""

    @attribute(label='CirrusState', dtype=str)
    def CirrusInfo(self):
        data = self.rga.cirrusInfo()
        print('CirrusInfo:[%d]\n %s' % (len(data),data))
        return data
    @command
    def CirrusPumpOn(self):
        return self.rga.cirrusPump(True)
    @command
    def CirrusPumpOff(self):
        return self.rga.cirrusPump(False)
    @command
    def CirrusCapillarHeaterOn(self):
        return self.rga.cirrusCapillaryHeater(True)
    @command
    def CirrusCapillarHeaterOff(self):
        return self.rga.cirrusCapillaryHeater(False)
    @command
    def CirrusHeaterOn(self):
        return self.rga.cirrusHeater(True)
    @command
    def CirrusHeaterOff(self):
        return self.rga.cirrusHeater(False)
    @command
    def cirrusValvePosition(self, value):
        return self.rga.cirrusValvePosition(value)

def main():

    MKSRGA.run_server()

if __name__ == "__main__":
    main()