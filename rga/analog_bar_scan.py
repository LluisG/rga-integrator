import logging
import threading
import time

import numpy as np

try:
    """for console"""
    from .record_scan import AnalogRecord
except:
    """for DS"""
    from record_scan import AnalogRecord


class AnalogBarScan:
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(threadName)s - '
                               ' %(levelname)s - %(message)s')

    def __init__(self, send):

        self.scan_th_stop = threading.Event()
        self.send = send
        log_name = '{}Record'.format(__name__)

        self.log = logging.getLogger(log_name)

        # self.start_mass = 0
        # self.end_mass = 1
        # self.filterMode = "PeakAverage"
        # self.accuracy = 2
        # self.gain = 0
        # self.source = 0
        # self.detector = 0
        #
        # self.bar_settings = [
        #     self.start_mass,
        #     self.end_mass,
        #     self.filterMode,
        #     self.accuracy,
        #     self.gain,
        #     self.source,
        #     self.detector
        # ]
        self.strToSend = ""

        self.record = AnalogRecord()

        self.recording = False

        self.masses_to_scan = np.array([])
        self.pressures_list = np.array([0, 0])

        self.lock = threading.Lock()

    def send_cmd(self, cmd):
        self.send.send(cmd)

    def start_scan(self, settings):

        strAux = ""
        self.scan_th_stop.clear()

        if settings.get("analog_bar") == "analog":
            del (settings["analog_bar"])
            settings["points_filter"] = int(settings["points_filter"])
            keys = 'start_mass','end_mass','points_filter','accuracy','gain','source','detector'
            for k in keys:
                strAux += str(settings.get(k,k)) + " "
            self.strToSend = "AddAnalog scan1 " + strAux
            self.masses_to_scan = np.arange(int(settings["start_mass"]),
                                            int(settings["end_mass"]) + 1,
                                            1 / int(settings["points_filter"]))


        else:
            del (settings["analog_bar"])
            keys = 'start_mass','end_mass','PeakAverage','accuracy','gain','source','detector'
            for k in keys:
                strAux += str(settings.get(k,k)) + " "

            self.strToSend = "AddBarChart scan1 " + strAux
            self.masses_to_scan = np.arange(int(settings["start_mass"]),
                                            int(settings["end_mass"]) + 1)

        scan_th = threading.Thread(target=self.scan_loop)
        scan_th.start()
        print('AnalogBarScan Started with Settings: \n' + self.strToSend)

    def scan_loop(self):
        self.pressures_list = np.zeros(len(self.masses_to_scan)) #+1)
        n_scan = 0
        r = self.send.read()

        print("Thread", self.scan_th_stop.isSet())
        while not self.scan_th_stop.isSet():
            n_mass = 0
            self.send_cmd(self.strToSend)
            with self.lock:
                self.pressures_list[0]=int(time.time())
            self.send_cmd('ScanAdd scan1')
            self.send_cmd('ScanStart 1')
            print(self.masses_to_scan)
            print(n_scan)
            
            #while not self.scan_th_stop.isSet() and not r.find("MassReading") > -1:
            #    r = self.send.read()
            #    n_mass = 0
            #    time.sleep((0.01))
            n_mass = 0
            while not self.scan_th_stop.isSet() and n_mass < len(self.masses_to_scan):
                while not self.scan_th_stop.isSet():
                    r = self.send.read(clean=False)
                    if 'MassReading' in r: 
                        break
                    else:
                        time.sleep((0.01))
                
                r = r.split(" ")
                #pressure = float("{:.2e}".format(float(r[2])))
                try:
                    pressure = float(r[-1]) if r[-1].strip() else 0
                except Exception as e:
                    import traceback
                    print('error: %s' % str(r[-1]))
                    traceback.print_exc()
                    raise e

                with self.lock:
                    self.pressures_list[n_mass] = pressure
                print("PRESSURES[%d]: %s" % (n_mass,self.pressures_list))
                print("nMass", n_mass)
                n_mass += 1

                self.log.info('Scan Done')
                time.sleep((0.06))

            self.send.send('ScanStop')
            n_scan += 1

        print("SCAN STOPPED!!")
        self.send.send('ScanStop')

        self.send_cmd('MeasurementRemoveAll')
        self.scan_th_stop.clear()

        # print("FullScan", self.get_analog_masses_pressures_scan())

    def get_masses(self):

        return self.masses_to_scan

    def get_real_time_pressures(self):
        return self.pressures_list

    def get_analog_masses_pressures_scan(self):

        a=self.pressures_list

        return a
        # return [self.masses_to_scan, self.pressures_list]

    def stopScan(self):
        print('Stopping Scan!')
        self.scan_th_stop.set()
        # self.masses_to_scan = np.array([])
        # self.pressures_list = np.array([0, 0])

    def time_scan(self):
        return time.time, self.pressures_list
