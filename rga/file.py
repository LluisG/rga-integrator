import datetime
import logging


def get_time():
    time = str(datetime.datetime.now())
    time = time.split(".")
    return time[0]


class File:
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(threadName)s - '
                               ' %(levelname)s - %(message)s')

    def __init__(self):
        log_name = '{}Record'.format(__name__)
        self.log = logging.getLogger(log_name)

    def create_file(self, filename):
        time = str(datetime.datetime.now())
        time = time.split(".")
        fl = str(filename), str(time[0])
        self.file_name = "".join(fl)
        self.log.info('File {a} created'.format(a=self.file_name))

        with open(self.file_name, "w+") as file:
            file.write(self.file_name + "\n")

        self.save_header()

    def save_header(self):
        pass

    def save_analog_line(self, line):
        with open(self.file_name, "a") as file:
            file.write("\n")
            file.write(get_time())
            file.write("\n")
            for i in line:
                file.write(i)

        self.log.info('Line attached')

    def get_gain(self, index):
        gains = ['1', '100', '20000']
        return gains[index]

    def get_detector(self, index):
        detectors = ['Faraday', 'Channeltron -650v', 'Channeltron -750v',
                     'Channeltron -900v']
        return detectors[index]
