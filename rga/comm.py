import telnetlib
import logging
import threading
import asyncio


class RgaTelnet:
    code = 'ascii'
    end_line = "\r\r".encode(code)
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(threadName)s - '
                               ' %(levelname)s - %(message)s')

    """TODO: reconnect after 3 empty responses"""
    def __init__(self, host, port=10014, timeout=3):
        log_name = '{}RgaTelnet'.format(__name__)
        self.log = logging.getLogger(log_name)


        self.host = host.encode(self.code)
        self.tl = telnetlib.Telnet(host, port, timeout)
        self.tl.read_until(self.end_line, 1)

        self.tl.write("Control telnet 1 \n".encode(self.code))
        self.log.info('Connect to %s:%s', host, port)
        self._lock = threading.Lock()

        """filament and cirrus control block"""

        # self.filStatusChange = Event()
        # self.cirrusTMPStame= Event()

        #read_tr = threading.Thread(target=self.read)
        #read_tr.start()




        """end CIRRUS TMP block"""
    def read(self, timeout=0.25, trace=True, clean=True):

        p = r = self.tl.read_until(self.end_line, timeout)
        #read until getting last answer
        while clean and r:
            r, p = self.tl.read_until(self.end_line, 0.025), r
        r = p
        r = r.decode(self.code)
        if trace:
            self.log.info('Read Message: {a}'.format(a=r))


        # if "FilamentStatus" in r:
        #     self.filStatusChange()
        #     return self.cleannswer(r)
        # elif "RVCPumpStatus" in r:
        #         self.cirrusTMPStame()
        #         r=r.split(" ")
        #         return r[1]
        # else:


        return r

    def cleannswer(self, r):
        a=r.split("\n")
        a=a.split("\t")
        return a[1]


    def send(self, cmd, clean=True):
        with self._lock:
            self.log.info('Command Send: {a} '.format(a=cmd))
            cmd = (cmd + "\n")
            cmd_enc = cmd.encode(self.code)
            self.tl.write(cmd_enc)
            r = self.read(timeout=0.025, trace=False, clean=clean)
            self.log.info('Returned Message: {a}'.format(a=r))
        return r



if __name__ == "__main__":
    rga = RgaTelnet("192.168.1.200")


    print(rga.send("1"))
    print(rga.send("2"))
    print(rga.send("3"))
    # time.sleep(1)11
    # print(rga.send_cmd("AddPeakJump PeakJump1 PeakCenter 5 0 0 0"))


    # print(rga.read())
    # rga.send_cmd("AddAnalog Analog1 1 10 4 5 0 0 0\r")
    # print(rga.read())
    # rga.send_cmd("ScanAdd Analog1\r")
    # rga.send_cmd("ScanStart 1\r")
    # while True:
    #     print(rga.read())
