import logging
import time

"""
     0-SerialNumber
     1-Name
     2-State
     3-UserApplication
     4-UserVersion
     5-UserAddress
     6-ProductID
     7-RFConfiguration
     8-DetectorType
     9-SEMSupply
     10-ExternalHardware
     11-TotalPressureGauge
     12-FilamentType
     13-ControlUnitUse
     14-SensorType
     15-InletType
     16-Version
     17-NumEGains
     18-NumDigitalPorts
     19-NumAnalogInputs
     20-NumAnalogOutputs
     21-NumSourceSettings
     22-NumInlets
     23-MaxMass
     24-ActiveFilament
     25-FullScaleADCAmps
     26-FullScaleADCCount
     27-PeakResolution
     28-ConfigurableIonSource
     29-RolloverCompensation
      
     30-ElectronEnergy
     31-IonEnergy
     32-ExtractVolts
     33-ElectronEmission
     34-LowMassAlignment
     35-HighMassAlignment
     36-LowMassResolution
     37-HighMassResolution
     38-MaxRecommendedPressure
     39-NumFaradayEGains
     40-NumMultEGains
     
     
    """

class Attributes:
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(threadName)s - '
                               ' %(levelname)s - %(message)s')

    def __init__(self, comm):

        self.send = comm
        # self.send_cmd("AddAnalog Analog1 1 50 32 5 1 2 2")

        self.general_info = {}

        self.info = {}
        self.update_info_dict()

        self.source_info_dict = {}
        self.update_source_info_dict()

        self.info.update(self.source_info_dict)

    def send_cmd(self, cmd):
        i = self.send.send(cmd)
        return i

    def update_source_info_dict(self):
        i = ""
        while not i.find("Source") > -1:
            i = self.send_cmd("SourceInfo 0")
            time.sleep(0.1)
        self.source_info_dict = self.parser(i)

        print(self.source_info_dict)
        self.info.update(self.source_info_dict)

    def update_info_dict(self):
        i = ""
        while not i.find("Info") > -1:
            i = self.send_cmd("Info")
            time.sleep(0.1)
        self.info = self.parser(i)

    def get_info(self):

        return self.info

    def parser(self,line):
        lines = line.split("\n")
        ret = {}
        for line in lines:
            try:
                k, v = line.split()[:2]
            except:
                # print("skipping line: '{}'".format(line))
                continue
            ret[k] = v
        return ret

if __name__ == "__main__":
    r = Attributes('192.168.1.200')
    print(r)